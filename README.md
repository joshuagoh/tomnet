<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- code_chunk_output -->

- [ToMnet+ project](#tomnet-project)
  - [Directory Structure](#directory-structure)
    - [models](#models)
    - [scripts](#scripts)
    - [Notes](#notes)

<!-- /code_chunk_output -->

# ToMnet+ project

- This work was published to ROMAN 2020 with Chuang Y. S., Hung H. Y., and Gamborino, E. as primary developers.

> Chuang, Y.-S., Hung, H.-Y., Gamborino, E., Goh, J. O. S., Huang, T.-R., Chang, Y.-L., Yeh, S.-L., & Fu, L.-C. (2020). Using Machine Theory of Mind to Learn Agent Social Network Structures from Observed Interactive Behaviors with Targets. 2020 29th IEEE International Conference on Robot and Human Interactive Communication (RO-MAN), 1013–1019. IEEE. https://doi.org/10.1109/RO-MAN47096.2020.9223453

- We extend from https://arxiv.org/abs/1802.07740.

> Rabinowitz, N., Perbet, F., Song, F., Zhang, C., Eslami, S. M. A., & Botvinick, M. (2018, July 3). Machine Theory of Mind. 80, 4218–4227.

## Directory Structure



### models

Contain the neural network models and also the model results


### scripts

Contain any scripts that are not the neural network model itself.


### Notes

server directory:
/bml/Data/Bank6/Robohon_YunShiuan/tomnet-project/
